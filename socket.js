const express = require("express");
const { createServer } = require("http");
const { Server } = require("socket.io");

const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, { 
    cors: {
        origin: "http://localhost:8000"
      }
 });

users = [];
io.on("connect", (socket) => {
    console.log('A user connected');
    socket.on('setUsername', function(data){
       console.log(data);
       if(users.indexOf(data) > -1){
          socket.emit('userExists', data + ' username is taken! Try some other username.');
       } else {
          users.push(data);
          socket.emit('userSet', {username: data});
       }
    });
    socket.on('msg', function(data){
       //Send message to everyone
       io.sockets.emit('newmsg', data);
    })
});

httpServer.listen(5000);